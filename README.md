## Map-wide decal templates
Various decal templates for map-wide decals that are garantueed to cover exactly the entire map. This is useful for map-wide lighting and / or normal maps to provide additional detail and realism to the terrain.

## Prerequisites
The templates as of is only supported with the old GPG editor. You can download it at:
 - https://wiki.faforever.com/index.php?title=Map_Editing_Tools

If you used the Ozone editor for your map previously then make sure you save it as `.v56` instead of `.v60`. You can find this setting under `Map -> Senario -> Other` and then tick the `Save as v56 (Vanilla)` checkbox.

## Resolution
A decal that is map-wide requires quite a hefty resolution. Ideally you'd want the following resolutions for a high-quality result:
 - 5x5   -> 4096x4096 (~20mb / texture)
 - 10x10 -> 8192x8192 (~80mb / texture)
 - 20x20 -> 16384x16384 (~320mb / texture)
 - 40x40 -> 32768x32768 (~1.2gb / texture)
 
However due to various limitations such as:
 - VRAM of your graphics card: a texture is stored uncompressed on your GPU.
 - Maximum supported texture size of your graphics card: a lot of graphic cards do not support textures larger than 8192.
 - Storage: a texture of size 4096x4096 is on its own already 20 mb on the server and on your local storage device, this will only increase with larger textures.

Therefore we do not recommend to use textures larger than 4096x4096.

## Short guide on usage
I assume that you already have a map-wide normal texture and map-wide lighting texture available. This short guide will not cover the creation of those textures. Choose the correct template depending on your map size. For this guide I assume you have chosen [10x10.lua](10x10.lua). Copy the file and edit it accordingly. The relevant fields are marked:

``` lua
Decals = {
    --
    ['9001'] = {
        type = 'Normals',
        name0 = '/maps/example-map.v0001/env/decals/normal.dds', -- relevant
        name1 = '',
        scale = { 512, 512, 512 }, 
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
    ['9002'] = {
        type = 'Albedo',
        name0 = '/maps/example-map.v0001/env/decals/lightmap.dds', -- relevant
        name1 = '',
        scale = { 512, 512, 512 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
}
```

Make sure that your decals are stored in the correct folder hierarchy: `env/decals`. Then store it with the corresponding name. Edit the relevant fields to incorporate your map name instead of `example-map.v0001`. 

## Frequently Asked Questions (FAQ)

### When I update my map the decal is all weird - what is going on?
When you update the map version and you do not change the path accordingly then the game will look for a texture at the wrong location. A typical error is changing the version of the map: this also changes the folder name and therefore the path in question. 

When you use the `save as new version` feature of the Ozone editor then all the textures are copied over and the paths are updated accordingly. This does not apply to water-related textures.

### When I load my map the screen freezes for a long period of time - what is going on?
The resolution (width / height) of the decals needs to be a power of 2. When this is the case there is almost no loading fee. If that is not the case I suspect that Supreme Commander applies some conversion in the background. This can take quite some time given the resolutions of the textures in question. Common 'to the power of two' numbers are:
 - 256, 512, 1024, 2048, 4096, 8192, 16384, 32768.

### I don't have a normal or lighting texture - can I remove one of the two?
Yes - you can freely remove one of them. Make sure you remove the everything between the comments to ensure the syntax of LUA is still formally correct. As an example say we want to remove the lighting texture from the example in the guide then it would be:

``` lua 
Decals = {
    --
    ['9001'] = {
        type = 'Normals',
        name0 = '/maps/example-map.v0001/env/decals/normal.dds', -- relevant
        name1 = '',
        scale = { 512, 512, 512 }, 
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
}
```

The comment sections (`--`) are there to guide you with the syntax in case you are not aware of it.
