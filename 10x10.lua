Decals = {
    --
    ['9001'] = {
        type = 'Normals',
        name0 = '/maps/example-map.v0001/env/decals/normal.dds',
        name1 = '',
        scale = { 512, 512, 512 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
    ['9002'] = {
        type = 'Albedo',
        name0 = '/maps/example-map.v0001/env/decals/lightmap.dds',
        name1 = '',
        scale = { 512, 512, 512 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
    --
}
